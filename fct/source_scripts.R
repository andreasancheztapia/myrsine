
  library(devtools)
  source_url("https://bitbucket.org/andreasancheztapia/enm-scripts/raw/c9918d15ff9e33c4be992ca07560b5e2bf520ae3/fct/modelos.R",sha1 = "a0a6bf5ee1973e69b220a6b92189994db8ba9c1c")
  
  
  source_url("https://bitbucket.org/andreasancheztapia/enm-scripts/raw/b627bf0553468f653c9b896bdaeb364517f7aad7/fct/final_model.R",sha1 = "300b8f58c79269e5f9daa794e56bd95d1472fbc0")
  
  source_url("https://bitbucket.org/andreasancheztapia/enm-scripts/raw/c9918d15ff9e33c4be992ca07560b5e2bf520ae3/fct/centroid.R",sha1 = "6ad808248c800d18980b4fe7f79dec40e373fd4f")
  
  source_url("https://bitbucket.org/andreasancheztapia/enm-scripts/raw/c9918d15ff9e33c4be992ca07560b5e2bf520ae3/fct/ensemble.R",sha1 = "3d4051759a21209f5ff75cfe62c3b76f5c7258fd")
  