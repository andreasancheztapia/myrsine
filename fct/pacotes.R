#1. carga paquetes####
ipak <- function(pkg){
    new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
    if (length(new.pkg))
        install.packages(new.pkg, dependencies = TRUE)
    sapply(pkg, require, character.only = TRUE)
    
}

ipak(c("devtools"
       ,"magrittr"
       ,"gdata"
       ,"maps"
       ,"raster"
       ,"maptools"
       ,"dismo"
       ,"vegan"
       ,"caret"
       ,"scales"
       ,"pander"
       ,"readxl"
       ,"pbkrtest"
       ,"caret"
       ,"sp"
       ,"dplyr"
       ,"stringr"
       ,"ggplot2"
       ,"data.table"
       ,"reshape2"
       ,"rgbif"
       ,"rgeos"
       ,"geosphere"
       ,"grDevices"
       ,"betapart"
       ,"rgdal" 
       ,"CommEcol"))
