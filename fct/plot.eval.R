plot.eval <- function(sp, 
                      mods.folder,
                      proj.folder,
                      eval.param = c("TSS","AUC"),
                      eval.threshold = 0.5) {
    evall <- list()
    a <- list.files(
        path = paste(mods.folder, sp, proj.folder, sep = "/"),
        pattern = paste0("evaluate"), full.names = T)
    for (k in seq_along(a))
        evall[[k]] <- read.table(a[[k]], header = T, row.names = 1)
    library(data.table)
    evall <- rbindlist(evall)
    evall <- as.data.frame(evall)
    evall$species <- rep(sp, nrow(evall))
    library(dplyr)
    eval <- dplyr::filter(evall, AUC > 0.5 & TSS > eval.threshold) %>% 
        dplyr::select(species, partition, algoritmo, AUC, TSS)
     for (q in seq_along(eval.param)) {
     pp <- eval.param[q]
         stripchart(evall[,pp] ~ evall$algoritmo, vertical = T, pch = 19, 
                    ylab = pp, ylim = c(0, 1),
                    main = sp,
                    las = 2)
         abline(h = eval.threshold, col = "red")
         }
    return(eval)
}