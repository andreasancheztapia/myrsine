#load("/Volumes/My Passport/myrsine /backup.RData")

library(raster)
plot(hull)
#No hay necesidad de volver a hacer los viejos
#ambiental
#png("./figs/overlap_AMBIENTAL_Broennimann.png", res = 300, height = 600 *300/72, width = 600 * 300/72)
# par(mfrow = c(13, 13), mar = c(0, 0, 0, 0))
# k <- 1
# for (i in 1:13) {
#     for (j in 1:13) {
#     if(i <= j) {
#         plot(hull, lwd = 0.1)
#         plot(hull[i], col = scales::alpha("red", 0.1), add = T)
#         plot(hull[j], col = scales::alpha("blue", 0.1), add = T)
#         if (i == j) text(0, 0, paste("M.",tabela$codigo[i]), font = 3)
#         if (i != j) text(0.2, -0.4, round(overlap.D[i,j],3), font = ifelse(overlap.D.div.p[i, j] <= 0.1, 2, 1))
#     }
#     if (i > j) {
#     plot(0, 0, type ="n", axes = F, box = F)
#     }
#     k <- k + 1
# }
# }
# dev.off()
#
# ##geografico
# library(maps)
# png("./figs/overlap_GEOGRAFICO_broenimann.png", res = 300, height = 600 *300/72, width = 600 * 300/72)
# par(mfrow = c(13, 13), mar = c(0,0,0,0))
# k <- 1
# for (i in 1:13) {
#     for (j in 1:13) {
#     if(i <= j) {
#         plot(ens.ma[[i]] > 0.5, legend = F, col = c(scales::alpha("white",0), scales::alpha("red",0.3)),font.main = 3, axes = F, box = F, xlim = c(-57,-35),ylim = c(-30, -5))
#         plot(ens.ma[[j]] > 0.5, legend = F, col = c(scales::alpha("white",0), scales::alpha("blue",0.3)), add = T)
#         map(,,, add = T, col = "grey80")
#         if (i != j) text(-40,-25, round(over.results.D.sdm[i,j],3), font = ifelse(cor.D.sdm.P[i, j] <= 0.05, 2, 1))
# if (i == j) text(-40, -25, paste("M.",tabela$codigo[i]), font = 3)
# }
# if (i>j) {
# plot(0, 0, type ="n", axes = F, box = F)
# #text(0,0, round(over.results.D.sdm[i,j],3), font = ifelse(cor.D.sdm.P[i, j] <= 0.05, 2, 1))
# }
#         k <- k + 1
#     }
# }
# dev.off()


library(maps)
png("./figs/overlap_AMBOS150.png", res = 150, height = 600 *150/72, width = 600 * 150/72)
par(mfrow = c(13, 13), mar = c(0, 0, 0, 0))
k <- 1
for (i in 1:13) {
    for (j in 1:13) {
    if (i == j) {
    plot(0, 0, type ="n", axes = F, box = F)
    text(0, 0, paste("M.",tabela$codigo[i]), font = 3)
    }
    if (i < j) {
        plot(hull, lwd = 0.1)
        plot(hull[i], col = scales::alpha("red", 0.1), add = T)
        plot(hull[j], col = scales::alpha("blue", 0.1), add = T)
        if (i == j) text(0, 0, paste("M.",tabela$codigo[i]), font = 3)
        if (i != j) text(0.25, -0.4, round(overlap.D[i,j],3), font = ifelse(overlap.D.div.p[i, j] <= 0.1, 2, 1))
    }
    if (i > j) {
        plot(ens.ma[[j]] > 0.5, legend = F, col = c(scales::alpha("white",0), scales::alpha("red",0.3)),font.main = 3, axes = F, box = F, xlim = c(-57,-35),ylim = c(-30, -5))
        plot(ens.ma[[i]] > 0.5, legend = F, col = c(scales::alpha("white",0), scales::alpha("blue",0.3)), add = T)
        map(,,, add = T, col = "grey80")
        text(-40,-25, round(D.sdm[j,i],3), font = ifelse(sdm.D.div.p[j, i] <= 0.05, 2, 1))
    }
    k <- k + 1
}
}
dev.off()
library(vegan)
mantel(as.dist(1 - over.results.D), as.dist(1 - over.results.D.sdm))
#orden.g <- c(2, 4, 11, 3, 9, 12, 13, 1, 5, 6, 7, 8, 10)
#for (i in orden.g) {
#    for (j in orden.g) {
ls()
