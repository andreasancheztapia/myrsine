library(ecospat)
spp <- ecospat.testNiche
sp1 <- spp[1:32,1:3]
plot(spp$long, spp$lat)
points(sp1$long, sp1$lat, col = "green")
occ.sp1 <- ecospat.occ.desaggregation(dfvar = sp1,
                                      colxy = 2:3,
                                      colvar = NULL,
                                      min.dist = 500,
                                      plot = F)
points(occ.sp1$long,occ.sp1$lat,col = "red")
clim <- ecospat.testData[2:8]
dim(spp)
dim(sp1)
dim(occ.sp1)
dim(clim)
#points(clim$long, clim$lat, col = "blue", pch = 3)

occ_sp1 <- ecospat.sample.envar(dfsp = occ.sp1,
                                colspxy = 1:2,
                                colspkept = 1:2,
                                dfvar = clim,
                                colvarxy = 1:2,
                                colvar = "all",
                                resolution = 25)
points(occ_sp1$long,occ_sp1$lat, col = "orange", pch = 19)
head(occ_sp1)
dim(occ_sp1)
occ_sp1 <- na.exclude(occ_sp1)
dim(occ_sp1)
dim(occ.sp1)
####volver a comenzar par ael grid ¬¬ ----
## Not run: 
spp <- ecospat.testNiche
clim <- ecospat.testData[2:8]

occ.sp_test <-
    na.exclude(
        ecospat.sample.envar(
            dfsp = spp,
            colspxy = 2:3,
            colspkept = 1:3,
            dfvar = clim,
            colvarxy = 1:2,
            colvar = "all",
            resolution = 25
        )
    )

occ.sp <- cbind(occ.sp_test, spp[, 4]) #add species names

# list of species
sp.list <- levels(occ.sp[, 1])
sp.nbocc <- c()

for (i in 1:length(sp.list)) {
    sp.nbocc <- c(sp.nbocc, length(which(occ.sp[, 1] == sp.list[i])))
}
#calculate the nb of occurences per species

sp.list <- sp.list[sp.nbocc > 4] # remove species with less than 5 occurences
nb.sp <- length(sp.list) #nb of species
ls()
# selection of variables to include in the analyses 
# try with all and then try only worldclim Variables
Xvar <- c(3:7)
nvar <- length(Xvar)

#number of interation for the tests of equivalency and similarity
iterations <- 100
#resolution of the gridding of the climate space
R <- 100
#################################### PCA-ENVIRONMENT ##################################
data <- rbind(occ.sp[, Xvar + 1], clim[, Xvar])
w <- c(rep(0, nrow(occ.sp)), rep(1, nrow(clim)))
pca.cal <- dudi.pca(data, row.w = w, center = TRUE, scale = TRUE, scannf = FALSE, nf = 2)

####### selection of species ######
sp.list
sp.combn <- combn(1:2,2)

for (i in 1:ncol(sp.combn)) {
    row.sp1 <- which(occ.sp[, 1] == sp.list[sp.combn[1, i]]) # rows in data corresponding to sp1
    row.sp2 <- which(occ.sp[, 1] == sp.list[sp.combn[2, i]]) # rows in data corresponding to sp2
    name.sp1 <- sp.list[sp.combn[1, i]]
    name.sp2 <- sp.list[sp.combn[2, i]]
    # predict the scores on the axes
    scores.clim <- pca.cal$li[(nrow(occ.sp) + 1):nrow(data), ]  #scores for global climate
    scores.sp1 <- pca.cal$li[row.sp1, ]					#scores for sp1
    scores.sp2 <- pca.cal$li[row.sp2, ]					#scores for sp2
}
plot(pca.cal$li, pch= 19)
points(scores.sp1, col = "red")
points(scores.sp2, col = "blue")
# calculation of occurence density and test of niche equivalency and similarity 
z1 <- ecospat.grid.clim.dyn(scores.clim, scores.clim, scores.sp1, R = 50)
z2 <- ecospat.grid.clim.dyn(scores.clim, scores.clim, scores.sp2,R = 100)
plot(z1$z, add = F)
#plot(pca.cal$li, pch= 19)
points(scores.sp1, col = "red")
plot(z2$z, add = F)
points(scores.sp2, col = "blue")
ecospat.plot.niche.dyn(z2)
a <-
    niche.equivalency.test(z1, z2, rep = 100)# test of niche equivalency and similarity according to Warren et al. 2008
b <- niche.similarity.test(z1, z2, rep = 100)