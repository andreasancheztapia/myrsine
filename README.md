Myrsine is a repository where all analysis of Sánchez-Tapia et al paper on the geographic distribution and niche width of thirteen Myrsine species of the Brazilian Atlantic Forest are being stored.

+ data: occurrence records, shapefiles 
+ env; predictor variables, 19 worldclim bioclimatic variables cut to the surface of the Brazilian Atlantic Forest at 10 x 10 km resolution. 
+ fct: holds the functions and scripts needed for analysis,
+ mods.mean holds the output models, created with a mean distance buffer. 
 
The whole script is in Myrsine geral limpo.R


